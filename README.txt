CKEditor Line Utilities
=======================

Description
===========
A set of utilities to find and create horizontal spaces in edited contents.

Installation
============

This module requires the core CKEditor module.

1. Download the plugin from http://ckeditor.com/addon/lineutils.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable CKEditor Line Utilities module in the Drupal admin.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.

MAINTAINERS
===========
Mauricio Dinarte - https://www.drupal.org/u/dinarcon

Credits
=======
Initial development and maintenance by Agaric.
