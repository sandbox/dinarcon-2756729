<?php

namespace Drupal\ckeditor_lineutils\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "lineutils" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lineutils",
 *   label = @Translation("Line Utilities Plugin"),
 * )
 */
class LineUtilsPlugin extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return base_path() . 'libraries/lineutils/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
